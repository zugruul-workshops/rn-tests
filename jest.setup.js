import React from 'react';
import RN from 'react-native';
import renderer from 'react-test-renderer';

jest.mock('NativeModules', () => ({
  StatusBarManager: {
    HEIGHT: 48,
    setStyle: jest.fn(),
    setHidden: jest.fn(),
    setNetworkActivityIndicatorVisible: jest.fn()
  },
  PlatformConstants: {
    forceTouchAvailable: false
  },
  UIManager: {
    setJSResponder: jest.fn()
  },
  RNGestureHandlerModule: {
    attachGestureHandler: jest.fn(),
    createGestureHandler: jest.fn(),
    dropGestureHandler: jest.fn(),
    updateGestureHandler: jest.fn(),
    State: {},
    Directions: {}
  }
}));

global.React = React;
global.RN = RN;

global.mockNativeModules = mockNativeModules;

global.renderer = renderer;
