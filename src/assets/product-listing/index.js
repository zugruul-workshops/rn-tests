export const shared = {
  increment: require('./increment.png'),
  decrement: require('./decrement.png')
};

export default shared;
