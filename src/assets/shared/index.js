export const shared = {
  react: require('./react.png'),
  fourall: require('./4all.png'),
  exit: require('./exit.png'),
  back: require('./back.png')
};

export default shared;
