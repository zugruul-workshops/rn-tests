import shared from './shared';
import productListing from './product-listing';

export const assets = {
  shared,
  productListing
};

export default assets;
