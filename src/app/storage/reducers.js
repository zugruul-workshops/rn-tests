import { combineReducers } from 'redux';
import loginReducer from 'containers/login/login.reducer';

const rootReducer = combineReducers({
  loginReducer
});

export default rootReducer;
