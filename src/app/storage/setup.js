import { createStore, applyMiddleware, compose } from 'redux';

// import { composeWithDevTools } from 'remote-redux-devtools';
import thunk from 'redux-thunk';

import rootReducer from './reducers';

export default (configureStore = () => {
  // const composeEnhancers = composeWithDevTools({ realtime: true, port: 8097 });
  const store = createStore(rootReducer, compose(applyMiddleware(thunk)));
  return store;
});
