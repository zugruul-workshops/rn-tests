import SetupNavigation from './navigation.setup';
import NavigationService from './navigation.service';

export { SetupNavigation, NavigationService };
export default NavigationService;
