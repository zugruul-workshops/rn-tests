export const routes = {
  // cart: 'Cart',
  forgotPassword: 'ForgotPassword',
  home: 'Home',
  login: 'Login',
  // productListing: 'ProductListing',
  signup: 'Signup'
};

export default routes;
