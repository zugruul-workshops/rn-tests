import SplashScreen from 'react-native-splash-screen';
import { StackActions, NavigationActions } from 'react-navigation';
import routes from './navigation.options';

let _navigator;

export const setTopLevelNavigator = navigatorRef => {
  _navigator = navigatorRef;
  setTimeout(() => SplashScreen.hide(), 1500);
};

export const navigate = (routeName, params) => {
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params
    })
  );
};

export const back = () => {
  _navigator.dispatch(NavigationActions.back());
};

export const replace = (routeName, params) => {
  _navigator.dispatch(
    StackActions.replace({
      routeName,
      params
    })
  );
};

export default {
  navigate,
  replace,
  back,
  setTopLevelNavigator,
  routes
};
