export default {
  navigate: () => jest.fn(),
  replace: () => jest.fn(),
  back: () => jest.fn(),
  setTopLevelNavigator: () => jest.fn(),
  routes: () => jest.fn()
};
