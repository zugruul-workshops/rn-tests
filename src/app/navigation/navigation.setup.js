import React from 'react';
import assets from 'assets';

import { Image, TouchableOpacity } from 'react-native';

import screens from 'containers/';
import routes from './navigation.options';

import { createStackNavigator, createAppContainer } from 'react-navigation';
import withBackgroundAndStatusBar from 'hocs/withBackgroundAndStatusBar';

const RootStack = createStackNavigator(
  {
    [routes.forgotPassword]: {
      screen: withBackgroundAndStatusBar(screens.forgotPassword)
    },
    [routes.home]: {
      screen: withBackgroundAndStatusBar(screens.home)
    },
    [routes.login]: {
      screen: withBackgroundAndStatusBar(screens.login)
    },
    [routes.signup]: {
      screen: withBackgroundAndStatusBar(screens.signup)
    }
  },
  {
    headerMode: 'float',
    initialRouteName: routes.login,
    defaultNavigationOptions: {
      headerTransparent: true,
      headerBackTitle: '',
      headerBackImage: <Image source={assets.shared.back} />
    }
  }
);

export default createAppContainer(RootStack);
