import api, { API_URL } from './api';

export { API_URL };

export default {
  ...api
};
