/** @flow */
import React from 'react';
import { sharedStyles } from 'shared';

import { View, Text, Dimensions, TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const { width, height } = Dimensions.get('screen');

class WideButton extends React.PureComponent {
  render() {
    const { gradient, text, onPress, SubComponent } = this.props;

    return (
      <View
        style={{
          flex: 1,
          alignSelf: 'center',
          minHeight: 41,
          marginBottom: 15
        }}
      >
        <TouchableOpacity
          onPress={onPress}
          style={{
            flexDirection: 'row',
            flex: 1,
            borderRadius: 5,
            maxHeight: width * 0.7 * 0.15,
            alignSelf: 'center'
          }}
        >
          <LinearGradient
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              borderWidth: 1,
              borderRadius: 5,
              borderColor: '#fff'
            }}
            colors={gradient || ['#1200DC', '#1B0030']}
          >
            <Text style={sharedStyles && sharedStyles.text.bigButtonText}>{text}</Text>
          </LinearGradient>
        </TouchableOpacity>
        {SubComponent}
      </View>
    );
  }
}

export default WideButton;
