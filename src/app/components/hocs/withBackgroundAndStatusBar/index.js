import withBackgroundAndStatusBar from './withBackgroundAndStatusBar';

export { withBackgroundAndStatusBar };

export default withBackgroundAndStatusBar;
