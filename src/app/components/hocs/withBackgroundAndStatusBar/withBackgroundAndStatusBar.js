/** @flow */
import React from 'react';

import { StatusBar, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const defaultStatusBarProps = {
  barStyle: 'light-content',
  translucent: true,
  backgroundColor: 'transparent'
};

const defaultGradientProps = {
  colors: ['#600081', '#FF008A'],
  style: { flex: 1 }
};

const withBackgroundAndStatusBar = (WrappedComponent, gradientProps, statusBarProps) => {
  if (!WrappedComponent) {
    console.warn('No component given to withBackgroundAndStatusBar.');
    return View;
  }
  class withBackgroundAndStatusBar extends React.Component {
    render() {
      return (
        <React.Fragment>
          <StatusBar {...defaultStatusBarProps} {...statusBarProps} />
          <LinearGradient {...defaultGradientProps} {...gradientProps}>
            <WrappedComponent />
          </LinearGradient>
        </React.Fragment>
      );
    }
  }
  withBackgroundAndStatusBar.displayName = `withBackgroundAndStatusBar(${WrappedComponent.displayName ||
    WrappedComponent.name ||
    'Component'})`;
  return withBackgroundAndStatusBar;
};

export default withBackgroundAndStatusBar;
