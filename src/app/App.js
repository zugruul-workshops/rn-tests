import React from 'react';

import { SetupNavigation as Navigator } from './navigation';
import NavigationService from './navigation';

import { Provider } from 'react-redux';
import configureStore from 'storage/setup';

const store = configureStore();

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Navigator
          ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
      </Provider>
    );
  }
}
