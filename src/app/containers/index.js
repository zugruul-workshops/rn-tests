import forgotPassword from './forgot-password/forgot-password.screen';
import home from './home/home.container';
import login from './login/login.container';
import signup from './signup/signup.screen';

export default {
  // cart,
  forgotPassword,
  home,
  login,
  // productListing,
  signup
};
