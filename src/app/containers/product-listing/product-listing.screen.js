/** @flow */
import React from 'react';
import assets from 'assets';

import { View, Text, Image, TouchableOpacity } from 'react-native';

class HomeScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text style={{ color: '#fff', fontSize: 32 }}>Home Screen</Text>
      </View>
    );
  }
}

export default HomeScreen;
