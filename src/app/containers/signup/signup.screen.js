/** @flow */
import React from 'react';
import assets from 'assets';
import { sharedStyles } from 'shared';

import NavigationService from 'navigation';

import {
  View,
  TextInput,
  Text,
  Image,
  ImageBackground,
  Dimensions,
  TouchableOpacity
} from 'react-native';
import WideButton from 'components/WideButton';

const { width, height } = Dimensions.get('screen');

class SignupScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: undefined,
      password: undefined
    };
  }

  onChange = ({ field, value }) => {
    this.setState({
      [field]: value
    });
  };

  render() {
    const { home, login } = NavigationService.routes;
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          source={assets.shared.react}
          resizeMode={'contain'}
          style={{ flex: 4, alignItems: 'center', justifyContent: 'flex-end' }}
        >
          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'center',
              justifyContent: 'center',
              paddingHorizontal: 64
            }}
          >
            <Text
              style={{
                flex: 5,
                marginLeft: 5,
                fontFamily: 'Montserrat',
                fontSize: 12,
                fontWeight: 'normal',
                fontStyle: 'normal',
                letterSpacing: 0,
                color: '#fcfcfc'
              }}
            >
              Bem vindo ao workshop de testes react-native
            </Text>
          </View>
        </ImageBackground>
        <View
          style={{
            flex: 2,
            marginTop: 20,
            justifyContent: 'flex-start',
            alignItems: 'center',
            alignSelf: 'center',
            paddingHorizontal: 64
          }}
        >
          <View
            style={{
              flexDirection: 'row',
              backgroundColor: '#fff',
              borderTopStartRadius: 10,
              borderTopEndRadius: 10
            }}
          >
            <TextInput
              name={'name'}
              onChangeText={text => this.onChange({ field: 'email', value: text })}
              placeholder={'Nome completo'}
              autoComplete={'name'}
              style={{ flex: 1, marginLeft: 10, minHeight: 41 }}
            />
          </View>
          <View style={{ flexDirection: 'row', backgroundColor: '#fff' }}>
            <TextInput
              name={'email'}
              onChangeText={text => this.onChange({ field: 'email', value: text })}
              placeholder={'Endereço de email'}
              autoComplete={'email'}
              style={{ flex: 1, marginLeft: 10, minHeight: 41 }}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              backgroundColor: '#fff',
              borderBottomStartRadius: 10,
              borderBottomEndRadius: 10
            }}
          >
            <TextInput
              secureTextEntry
              name={'password'}
              onChangeText={text => this.onChange({ field: 'password', value: text })}
              placeholder={'Senha'}
              autoComplete={'off'}
              style={{ flex: 1, marginLeft: 10, minHeight: 41 }}
            />
          </View>
        </View>
        <View style={{ flex: 1, paddingHorizontal: 64 }}>
          <WideButton
            text={'Registrar'}
            onPress={() => console.warn('do auth process') & NavigationService.navigate(login)}
            SubComponent={
              <TouchableOpacity
                onPress={() => NavigationService.replace(login)}
                style={{ alignSelf: 'flex-start', marginTop: 3 }}
              >
                <Text style={sharedStyles.text.authFluxTextButtons}>Já tem uma conta?</Text>
              </TouchableOpacity>
            }
          />
        </View>
      </View>
    );
  }
}

export default SignupScreen;
