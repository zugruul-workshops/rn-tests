/** @flow */
import React from 'react';
import assets from 'assets';
import { sharedStyles } from 'shared';

import NavigationService from 'navigation';

import {
  View,
  TextInput,
  Text,
  Image,
  ImageBackground,
  Dimensions,
  TouchableOpacity
} from 'react-native';
import WideButton from 'components/WideButton';

const { width, height } = Dimensions.get('screen');

class ForgotPasswordScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: undefined,
      password: undefined
    };
  }

  onChange = ({ field, value }) => {
    this.setState({
      [field]: value
    });
  };

  render() {
    const { login } = NavigationService.routes;
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          source={assets.shared.react}
          resizeMode={'contain'}
          style={{ flex: 4, alignItems: 'center', justifyContent: 'flex-end' }}
        >
          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'center',
              justifyContent: 'center',
              paddingHorizontal: 64
            }}
          >
            <Text
              style={{
                flex: 5,
                marginLeft: 5,
                fontFamily: 'Montserrat',
                fontSize: 12,
                fontWeight: 'normal',
                fontStyle: 'normal',
                letterSpacing: 0,
                color: '#fcfcfc'
              }}
            >
              Esqueceu sua senha? Nós te ajudamos ;)
            </Text>
          </View>
        </ImageBackground>
        <View
          style={{
            flex: 2,
            marginTop: 10,
            justifyContent: 'flex-start',
            alignItems: 'center',
            alignSelf: 'center',
            paddingHorizontal: 64
          }}
        >
          <View style={{ flexDirection: 'row', backgroundColor: '#fff', borderRadius: 10 }}>
            <TextInput
              name={'email'}
              onChangeText={text => this.onChange({ field: 'email', value: text })}
              placeholder={'Endereço de email'}
              style={{ flex: 1, marginLeft: 10, minHeight: 41 }}
            />
          </View>
        </View>
        <View style={{ flex: 1, paddingHorizontal: 64 }}>
          <WideButton
            text={'Solicitar troca de senha'}
            onPress={() =>
              console.warn('do forgot my password process') & NavigationService.replace(login)
            }
            SubComponent={
              <TouchableOpacity
                onPress={() => NavigationService.replace(login)}
                style={{ alignSelf: 'flex-start', marginTop: 3 }}
              >
                <Text style={sharedStyles.text.authFluxTextButtons}>
                  Lembrou? então clica aqui ;)
                </Text>
              </TouchableOpacity>
            }
          />
        </View>
      </View>
    );
  }
}

export default ForgotPasswordScreen;
