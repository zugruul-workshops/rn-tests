import React from 'react';

import HomeScreen from './home.screen';

import { connect } from 'react-redux';
import { doLogout } from 'containers/login/login.actions';

export class HomeContainer extends React.PureComponent {
  render() {
    const {
      loginReducer: {
        token,
        loggedUser: { name }
      }
    } = this.props;
    return <HomeScreen name={name} doLogout={this.props.doLogout} />;
  }
}

const mapStateToProps = state => {
  return {
    ...state
  };
};

const mapDispatchToProps = dispatch => {
  return {
    doLogout: () => dispatch(doLogout())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeContainer);
