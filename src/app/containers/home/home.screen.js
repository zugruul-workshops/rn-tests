/** @flow */
import React from 'react';
import assets from 'assets';

import {
  View,
  FlatList,
  TextInput,
  Text,
  Image,
  ImageBackground,
  Dimensions,
  TouchableOpacity
} from 'react-native';
import WideButton from 'components/WideButton';

const { width, height } = Dimensions.get('screen');

class HomeScreen extends React.Component {
  render() {
    const { name, doLogout } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          source={assets.shared.react}
          resizeMode={'contain'}
          style={{ flex: 4, alignItems: 'center', justifyContent: 'flex-end' }}
        >
          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'center',
              justifyContent: 'center',
              paddingHorizontal: 64
            }}
          >
            <Text
              style={{
                flex: 5,
                marginLeft: 10,
                fontFamily: 'Montserrat',
                fontSize: 24,
                fontWeight: 'normal',
                fontStyle: 'normal',
                letterSpacing: 0,
                color: '#fcfcfc',
                textAlign: 'center'
              }}
            >
              Bem vindo,{'\n'}
              <Text style={{ fontWeight: '600' }}>{name}</Text>
            </Text>
          </View>
        </ImageBackground>
        <View style={{ flex: 3, marginTop: 0 }}>
          <FlatList
            data={['Ver carrinho', ...[{ name: 'Loja 1' }, { name: 'Loja 2' }, 'Sair']]}
            keyExtractor={(_, index) => String(index)}
            renderItem={({ item, index }) => {
              const { doLogout } = this.props;
              let props = {};
              if (item === 'Sair') {
                props.gradient = ['#DC0000', '#7D0000'];
                props.onPress = doLogout;
              }

              if (item === 'Ver carrinho') {
                props.onPress = () => console.warn('Implementar navegação magrão');
              }

              return (
                <View style={{ minHeight: 41, width: width - 128 }}>
                  <WideButton
                    text={item.name || item}
                    onPress={() => console.warn('bora implementar magrão', index)}
                    {...props}
                  />
                </View>
              );
            }}
            contentContainerStyle={{
              alignItems: 'center',
              justifyContent: 'flex-start',
              paddingHorizontal: 64
            }}
            style={{
              marginTop: 20,
              alignSelf: 'center'
            }}
          />
        </View>
      </View>
    );
  }
}

export default HomeScreen;
