/** @flow */
import React from 'react';
import assets from 'assets';
import { sharedStyles } from 'shared';

import {
  View,
  TextInput,
  Text,
  Image,
  ImageBackground,
  Dimensions,
  TouchableOpacity
} from 'react-native';
import WideButton from 'components/WideButton';

const { width, height } = Dimensions.get('screen');

class LoginScreen extends React.PureComponent {
  render() {
    const { doLogin, onChange, navigateToSignup, navigateToForgotPassword } = this.props;
    const {
      login: { email, password }
    } = this.props;

    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          source={assets.shared.react}
          resizeMode={'contain'}
          style={{ flex: 4, alignItems: 'center', justifyContent: 'flex-end' }}
        >
          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'center',
              justifyContent: 'center',
              maxWidth: width * 0.7
            }}
          >
            <Image source={assets.shared.fourall} resizeMode={'contain'} style={{ flex: 1 }} />
            <Text
              style={{
                flex: 5,
                marginLeft: 10,
                fontFamily: 'Montserrat',
                fontSize: 12,
                fontWeight: 'normal',
                fontStyle: 'normal',
                letterSpacing: 0,
                color: '#fcfcfc'
              }}
            >
              Bem vindo ao workshop de testes react-native
            </Text>
          </View>
        </ImageBackground>
        <View
          style={{
            flex: 2,
            marginTop: 20,
            justifyContent: 'flex-start',
            alignItems: 'center',
            alignSelf: 'center',
            paddingHorizontal: 64
          }}
        >
          <View
            style={{
              flexDirection: 'row',
              backgroundColor: '#fff',
              borderTopStartRadius: 10,
              borderTopEndRadius: 10
            }}
          >
            <TextInput
              name={'email'}
              onChangeText={text => onChange({ field: 'email', value: text })}
              placeholder={'Endereço de Email'}
              autoComplete={'email'}
              value={email}
              style={{ flex: 1, marginLeft: 10, minHeight: 41 }}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              backgroundColor: '#fff',
              borderBottomStartRadius: 10,
              borderBottomEndRadius: 10
            }}
          >
            <TextInput
              secureTextEntry
              name={'password'}
              onChangeText={text => onChange({ field: 'password', value: text })}
              placeholder={'Senha'}
              autoComplete={'password'}
              value={password}
              style={{ flex: 1, marginLeft: 10, minHeight: 41 }}
            />
          </View>
          <TouchableOpacity
            onPress={() => navigateToForgotPassword()}
            style={{ alignSelf: 'flex-end', marginTop: 3 }}
          >
            <Text style={sharedStyles.text.authFluxTextButtons}>Esqueceu sua senha?</Text>
          </TouchableOpacity>
        </View>
        <View style={{ flex: 1, paddingHorizontal: 64 }}>
          <WideButton
            text={'Entrar'}
            onPress={() => {
              doLogin(email, password);
            }}
            SubComponent={
              <TouchableOpacity
                onPress={() => navigateToSignup()}
                style={{ alignSelf: 'flex-start', marginTop: 3 }}
              >
                <Text style={sharedStyles.text.authFluxTextButtons}>Ainda não tem conta?</Text>
              </TouchableOpacity>
            }
          />
        </View>
      </View>
    );
  }
}

export default LoginScreen;
