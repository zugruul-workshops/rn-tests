import React from 'react';

import LoginScreen from './login.screen';

import { connect } from 'react-redux';
import { doLogin } from './login.actions';

import NavigationService from 'navigation';

export class LoginContainer extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: undefined,
      password: undefined
    };
  }

  onChange = ({ field, value }) => {
    this.setState({
      [field]: value
    });
  };

  render() {
    return (
      <LoginScreen
        onChange={this.onChange}
        doLogin={this.props.doLogin}
        login={this.state}
        navigateToSignup={() => {
          const { signup } = NavigationService.routes;
          NavigationService.replace(signup);
        }}
        navigateToForgotPassword={() => {
          const { forgotPassword } = NavigationService.routes;
          NavigationService.replace(forgotPassword);
        }}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    login: state
  };
};

const mapDispatchToProps = dispatch => {
  return {
    doLogin: (email, password) => dispatch(doLogin(email, password))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginContainer);
