import { API_URL } from 'config';
import axios from 'axios';
axios.defaults.timeout = 3000;

import {
  createAuthentication,
  createAuthenticationSuccess,
  createAuthenticationFailure,
  createAuthenticationLogout
} from './login.action-creators';

import NavigationService from 'navigation';

export const doLogin = (email, password) => async dispatch => {
  __DEV__ && console.log('[Auth] Doing logon for user with email: ', email);
  dispatch(createAuthentication());
  try {
    const response = await axios.post(
      `${API_URL}/api/login`,
      { email: email, password: password },
      { headers: { 'Content-Type': 'application/json' } }
    );
    const responseData = response.data;
    const { statusDesc, data } = responseData;
    if (data) {
      __DEV__ && console.log('[Auth] Response: ', data);
      const { token, userData } = data;
      if (token && userData) {
        __DEV__ && console.log('[Auth] Obtained token: ', token);
        __DEV__ && console.log('[Auth] For user data: ', userData);
        dispatch(createAuthenticationSuccess(token, userData));
        const { home } = NavigationService.routes;
        NavigationService.replace(home);
      } else {
        alert(statusDesc);
        console.log('[Auth] Failed authentication: ', statusDesc);
        dispatch(createAuthenticationFailure());
      }
    } else {
      alert(statusDesc);
      console.log('[Auth] Error data is:', statusDesc);
      dispatch(createAuthenticationFailure());
    }
  } catch (error) {
    alert('Ocorreu um erro: ', error);
    console.log('[Auth] Error: ', error);
    dispatch(createAuthenticationFailure());
  }
};

export const doLogout = () => dispatch => {
  dispatch(createAuthenticationLogout());
  const { login } = NavigationService.routes;
  NavigationService.replace(login);
};
