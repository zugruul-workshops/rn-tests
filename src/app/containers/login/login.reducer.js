import actions from './login.action-types';

export const initialState = {
  token: null,
  loggedUser: null,
  isDoingAuth: false,
  error: false
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.AUTHENTICATION:
      return {
        ...state,
        token: null,
        loggedUser: null,
        isDoingAuth: true
      };
    case actions.AUTHENTICATION_SUCCESS:
      return {
        ...state,
        token: action.token,
        loggedUser: action.loggedUser,
        isDoingAuth: false
      };
    case actions.AUTHENTICATION_FAILURE:
      return {
        ...state,
        token: null,
        loggedUser: null,
        isDoingAuth: false,
        error: true
      };
    case actions.AUTHENTICATION_LOGOUT:
      return {
        ...state,
        token: null,
        loggedUser: null,
        isDoingAuth: false,
        error: false
      };
    default:
      return state;
  }
};

export default authReducer;
