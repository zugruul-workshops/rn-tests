import {
  AUTHENTICATION,
  AUTHENTICATION_SUCCESS,
  AUTHENTICATION_FAILURE,
  AUTHENTICATION_LOGOUT
} from './login.action-types';

export const createAuthentication = () => {
  return {
    type: AUTHENTICATION
  };
};

export const createAuthenticationSuccess = (token, userData) => {
  return {
    type: AUTHENTICATION_SUCCESS,
    token,
    loggedUser: userData
  };
};

export const createAuthenticationFailure = () => {
  return {
    type: AUTHENTICATION_FAILURE
  };
};

export const createAuthenticationLogout = () => {
  return {
    type: AUTHENTICATION_LOGOUT
  };
};

export default {
  createAuthentication,
  createAuthenticationSuccess,
  createAuthenticationFailure,
  createAuthenticationLogout
};
