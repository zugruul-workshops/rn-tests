import creators from 'containers/login/login.action-creators';
import reducer from 'containers/login/login.reducer';
import types from 'containers/login/login.action-types';

import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';

const fakeToken =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YzU5ZDBlM2UwZGVjOTRhOTVmMmI4MmYiLCJuYW1lIjoibGVvbmFyZG8udmllaXJhQDRhbGwuY29tIiwiZW1haWwiOiJsZW9uYXJkby52aWVpcmFANGFsbC5jb20iLCJjcmVhdGVkQXQiOiIyMDE5LTAyLTA1VDE4OjA3OjMxLjA5M1oiLCJ1cGRhdGVkQXQiOiIyMDE5LTAyLTA1VDE4OjA3OjMxLjA5M1oiLCJpZCI6IjVjNTlkMGUzZTBkZWM5NGE5NWYyYjgyZiIsImlhdCI6MTU0OTM5MDEyMSwiZXhwIjoxNTQ5MzkzNzIxfQ.zSnIZPhzfsa3vgjSQUCnlDA9g-6mZMXduk31LNxo8_A';
const fakeLoggedUser = {
  _id: '5c59d0e3e0dec94a95f2b82f',
  name: 'Evangivaldo Pereira',
  email: 'evangivaldo.pereira@gmail.com',
  createdAt: '2019-02-05T18:07:31.093Z',
  updatedAt: '2019-02-05T18:07:31.093Z',
  id: '5c59d0e3e0dec94a95f2b82f'
};

/**
 * @creators
 * @types
 */
describe('login action creators', () => {
  it('should create an AUTHENTICATION action', () => {
    const expectedAction = {
      type: types.AUTHENTICATION
    };
    expect(creators.createAuthentication()).toEqual(expectedAction);
  });

  it('should create an AUTHENTICATION_SUCCESS action with token and loggedUser', () => {
    const expectedAction = {
      type: types.AUTHENTICATION_SUCCESS,
      token: fakeToken,
      loggedUser: fakeLoggedUser
    };
    expect(creators.createAuthenticationSuccess(fakeToken, fakeLoggedUser)).toEqual(expectedAction);
  });

  it('should create an AUTHENTICATION_FAILURE action', () => {
    const expectedAction = {
      type: types.AUTHENTICATION_FAILURE
    };
    expect(creators.createAuthenticationFailure()).toEqual(expectedAction);
  });

  it('should create an AUTHENTICATION_LOGOUT action', () => {
    const expectedAction = {
      type: types.AUTHENTICATION_LOGOUT
    };
    expect(creators.createAuthenticationLogout()).toEqual(expectedAction);
  });
});

/**
 * @reducer
 * @types
 * @initialState
 */
describe('login reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      token: null,
      loggedUser: null,
      isDoingAuth: false,
      error: false
    });
  });

  it('should handle AUTHENTICATION', () => {
    expect(
      reducer(
        {},
        {
          type: types.AUTHENTICATION
        }
      )
    ).toEqual({
      token: null,
      loggedUser: null,
      isDoingAuth: true
    });
  });

  it('should handle AUTHENTICATION_SUCCESS', () => {
    expect(
      reducer(
        {},
        {
          type: types.AUTHENTICATION_SUCCESS,
          token: fakeToken,
          loggedUser: fakeLoggedUser
        }
      )
    ).toEqual({
      token: fakeToken,
      loggedUser: fakeLoggedUser,
      isDoingAuth: false
    });
  });

  it('should handle AUTHENTICATION_FAILURE', () => {
    expect(
      reducer([], {
        type: types.AUTHENTICATION_FAILURE
      })
    ).toEqual({
      token: null,
      loggedUser: null,
      isDoingAuth: false,
      error: true
    });
  });

  it('should handle AUTHENTICATION_LOGOUT', () => {
    expect(
      reducer(
        {
          token: fakeToken,
          loggedUser: fakeLoggedUser,
          isDoingAuth: false,
          error: false
        },
        {
          type: types.AUTHENTICATION_LOGOUT
        }
      )
    ).toEqual({
      token: null,
      loggedUser: null,
      isDoingAuth: false,
      error: false
    });
  });

  it('should handle invalid action types by not changing the state', () => {
    expect(
      reducer(
        {
          token: fakeToken,
          loggedUser: fakeLoggedUser,
          isDoingAuth: false,
          error: false
        },
        {
          type: 'AUTHENTICATION_INVALID_TYPE'
        }
      )
    ).toEqual({
      token: fakeToken,
      loggedUser: fakeLoggedUser,
      isDoingAuth: false,
      error: false
    });
  });
});

/**
 * @mockStore
 */
const middlewares = [thunk];
const mockStore = configureStore(middlewares);

it('should dispatch action', () => {
  const initialState = {
    token: null,
    loggedUser: null,
    isDoingAuth: false,
    error: false
  };

  const store = mockStore(initialState);

  store.dispatch(creators.createAuthentication());
  store.dispatch(creators.createAuthenticationFailure());

  const actions = store.getActions();
  const expectedPayloads = [{ type: 'AUTHENTICATION' }, { type: 'AUTHENTICATION_FAILURE' }];
  expect(actions).toEqual(expectedPayloads);
});
