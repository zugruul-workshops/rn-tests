import creators from 'containers/login/login.action-creators';
import reducer from 'containers/login/login.reducer';
import types from 'containers/login/login.action-types';

import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import LoginContainer from 'containers/login/login.container';

/**
 * @mockStore
 */
const middlewares = [thunk];
const mockStore = configureStore(middlewares);

it('container should match snapshot', () => {
  const initialState = {
    token: null,
    loggedUser: null,
    isDoingAuth: false,
    error: false
  };

  const store = mockStore(initialState);
  const testRenderer = renderer.create(
    <Provider store={store}>
      <LoginContainer />
    </Provider>
  );
  const testJSON = testRenderer.toJSON();
  expect(testJSON).toMatchSnapshot();
});
