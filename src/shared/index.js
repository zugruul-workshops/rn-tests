export const sharedStyles = {
  image: { ...require('./image').styles },
  text: { ...require('./text').styles }
};

export default { sharedStyles };
