import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  mediumShadow: {
    width: 375,
    height: 262,
    shadowColor: 'rgba(0, 0, 0, 0.25)',
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 4,
    shadowOpacity: 1
  }
});

export default styles;
