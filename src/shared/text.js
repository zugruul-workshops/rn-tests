import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  bigButtonText: {
    fontFamily: 'Montserrat',
    fontSize: 16,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    color: '#ffffff'
  },
  authFluxTextButtons: {
    fontFamily: 'Montserrat',
    fontSize: 14,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    color: '#fcfcfc'
  }
});

export default styles;
