module.exports = {
  preset: 'react-native',
  testEnvironment: 'jsdom',
  moduleFileExtensions: ['ios.js', 'native.js', 'js', 'json', 'jsx', 'node'],
  // testRegex: '/__tests__/.*\\.spec\\.js$',
  testRegex: '.*\\.spec\\.js$',
  transform: {
    '^.+\\.js$': '<rootDir>/node_modules/react-native/jest/preprocessor.js',
    '^.+\\.jsx$': '<rootDir>/node_modules/react-native/jest/preprocessor.js'
  },
  setupFiles: ['./jest.setup.js'],
  coveragePathIgnorePatterns: ['node_modules/'],
  transformIgnorePatterns: [
    'node_modules/(?!react-native|react-navigation|@react-navigation|react-native-linear-gradient)'
  ],
  globals: {
    React: true,
    RN: true,
    renderer: true,
    mockNativeModules: true
  }
};
