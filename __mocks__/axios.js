export default {
  defaults: {
    adapter: () => jest.fn(),
    timeout: 0,
    maxContentLength: -1,
    xsrfCookieName: 'XSRF-TOKEN',
    xsrfHeaderName: 'X-XSRF-TOKEN',
    transformResponse: [() => jest.fn((data, headers) => data), () => jest.fn(data)],
    headers: {
      common: {
        Accept: 'application/json, text/plain, */*'
      }
    },
    validateStatus: status => jest.fn(() => status >= 200 && status < 300)
  },
  get: jest.fn(() => Promise.resolve({ data: {} })),
  post: jest.fn((url, data, options) => {
    if (url === 'api/login') {
      if (data && data.email === 'evangivaldo.pereira@gmail.com' && data.password === 'senha') {
        return Promise.resolve({
          statusCode: 0,
          statusDesc: 'Success',
          data: {
            token: 'abcd&'
          }
        });
      }

      return Promise.resolve({
        statusCode: 2,
        statusDesc: 'Usuário ou senha inválidos',
        data: null
      });
    }

    return Promise.reject(`Cannot POST ${url}`);
  })
};
