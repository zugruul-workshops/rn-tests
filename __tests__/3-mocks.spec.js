import axios from 'axios';

/**
 * + https://jestjs.io/docs/en/manual-mocks
 * + https://www.leighhalliday.com/mocking-axios-in-jest-testing-async-functions
 */
it('mock axios', async () => {
  /** see __mocks__ */
  const response = await axios.post('api/login', {
    email: 'evangivaldo.pereira@gmail.com',
    password: 'senha'
  });
  expect(response).toEqual({ statusCode: 0, statusDesc: 'Success', data: { token: 'abcd&' } });
});
