/**
 * @describe cria um bloco que agrupa vários testes
 * @test @it fn que define o escopo do teste. it é um alias para test
 * @source https://jestjs.io/docs/en/api#describename-fn
 * @part {1}
 * - Dado um aglomerado de testes (describe)
 * - Dado uma fn hello que retorna a String 'world' experar esse retorno dela usando a fn global `test` e usando o alias da mesma `it`
 * @part {2}
 * - mudar a função it para it.only ou para test.only e ver o que acontece
 */

describe('aglomerado de testes', () => {
  const hello = 'world';

  test('basic test', () => {
    expect(hello).toBe('world');
  });

  it('basic it', () => {
    expect(hello).toBe('world');
  });
});

/**
 * @spies são capazes de obter informação sobre como foram chamados.
 * @source https://jestjs.io/docs/en/mock-functions.html
 * @source https://sinonjs.org/releases/v7.2.3/spies/
 * - criar spy usando jest.spyOn(obj, 'fn name'), dê o nome de greetingSpy a essa referencia
 * - executar a função duas vezes para os nomes 'Alejandro' e 'Evangivaldo'
 * - testar:
 *    + se ela retornou
 *    + primeiro arg da primeira execução
 *    + primeiro arg da segunda execução
 *    + o que ela retornou na primeira execução
 *    + o que ela retornou na segunda execução
 *    + quantas vezes retornou algo
 *    + no final dê um .mockRestore(); no greetingSpy criado
 */

const talker = {
  greet: name => console.warn(`Hello ${name}`) || `Hello ${name}`
};

it('spy fn', () => {
  const greetingSpy = jest.spyOn(talker, 'greet');

  talker.greet('Alejandro');
  talker.greet('Evangivaldo');

  expect(greetingSpy).toHaveBeenCalled();
  expect(greetingSpy.mock.calls[0][0]).toBe('Alejandro'); // espera-se que da primeira execução da função o primeiro arg tenha sido Alejandro
  expect(greetingSpy.mock.calls[1][0]).toBe('Evangivaldo'); // espera-se que da primeira execução da função o primeiro arg tenha sido Alejandro
  expect(greetingSpy.mock.results[0].value).toBe('Hello Alejandro');
  expect(greetingSpy).toHaveReturnedTimes(2);
  greetingSpy.mockRestore();
});

/**
 * @mocks são capazes de serem programados para atender expectativas. No jest também possuem as funcionalidades de spies.
 * @source https://jestjs.io/docs/en/mock-functions.html
 * - Criar uma função usando jest.fn(() => ~) que realiza a soma entre dois numeros
 * - Chamar ela duas vezes com número arbitrários
 * - Averiguar se:
 *    + foi chamada duas vezes (usando .mock.calls.length)
 *    + os argumentos ao qual foram utilizados na primeira e segunda execuções da função (usando .mock.calls[x][y] onde x é a execução da função e y a posição do arg passado)
 *    + o resultado da primeira e segunda execuções (usando .mock.results[x].value)
 */
it('mock fn', () => {
  const mockedFunction = jest.fn((x, y) => x + y);
  mockedFunction(1, 3);
  mockedFunction(-3, 4);

  expect(mockedFunction.mock.calls.length).toBe(2); // number of times it was called

  expect(mockedFunction.mock.calls[0][0]).toBe(1); // 1st fn 1st arg
  expect(mockedFunction.mock.calls[0][1]).toBe(3); // 1st fn 2st arg
  expect(mockedFunction.mock.results[0].value).toBe(4); // return 1st fn

  expect(mockedFunction.mock.calls[1][0]).toBe(-3); // 1st fn 1st arg
  expect(mockedFunction.mock.calls[1][1]).toBe(4); // 2nd fn 2nd arg
  expect(mockedFunction.mock.results[1].value).toBe(1); // return 2nd fn
});

/**
 * @stubs dão respostas a chamadas feitas durante testes. Respondendo conforme programado.
 * + Usando jest.fn() criar um stub que retorne em cada chamada feita (consecutiva) os seguintes:
 *    - hello -> world -> my -> name -> is -> evangivaldo (use o .mockReturnValueOnce)
 *    - todas as chamadas após evangivaldo devem retornar undefined (use o .mockReturnValue )
 *
 */

it('stub fn', () => {
  const stub = jest.fn();
  stub
    .mockReturnValueOnce('hello')
    .mockReturnValueOnce('world')
    .mockReturnValueOnce('my')
    .mockReturnValueOnce('name')
    .mockReturnValueOnce('is')
    .mockReturnValueOnce('evangivaldo')
    .mockReturnValue(undefined);

  const responses = [stub(), stub(), stub(), stub(), stub(), stub(), stub()];

  expect(stub).toHaveBeenCalledTimes(7);

  expect(responses[0]).toBe('hello');
  expect(responses[1]).toBe('world');
  expect(responses[2]).toBe('my');
  expect(responses[3]).toBe('name');
  expect(responses[4]).toBe('is');
  expect(responses[5]).toBe('evangivaldo');
  expect(responses[6]).toBeUndefined();
});

/**
 * @Fontes
 *  + https://sinonjs.org/releases/v7.2.3/
 *  + https://walde.co/2017/02/05/testes-e-javascript-diferenca-entre-fake-spy-stub-e-mock/
 *  + https://martinfowler.com/articles/mocksArentStubs.html
 *  + https://jestjs.io/docs/en/manual-mocks
 */
