import LinearGradient from 'react-native-linear-gradient';

/**
 * Galera... eu coloquei o React-Native no escopo global. Mals
 */

class TestComponent extends React.PureComponent {
  render() {
    const { onPress, onLongPress, text, textStyle, touchableOpacityStyle, Container } = this.props;
    const ContainerSelector = Container || RN.View;
    return (
      <RN.TouchableOpacity
        testID={'#touchable'}
        style={touchableOpacityStyle}
        onPress={e => onPress(e)}
        onLongPress={e => onLongPress}
      >
        <ContainerSelector testID={'#container'}>
          <RN.Text testID={'#text'} style={textStyle}>
            {text || 'Click here!'}
          </RN.Text>
        </ContainerSelector>
      </RN.TouchableOpacity>
    );
  }
}

/**
 * @toMatchSnapshot
 * - dentro do aglomerado de testes abaixo:
 *    - Crie um teste que realize um teste de Snapshot do componente acima com o texto 'Hey! Press me!'
 *    - Crie um teste que realize um teste de Snapshot do componente acima com os props default
 *
 * + para tal utilize a função renderer (ver jest.setup.js, ela foi fornecido ao contexto global)
 * + render.create(...).toJSON();
 * + expect(esse carinha ai encima).toMatchSnapshot();
 */

describe('<TestComponent/>', () => {
  it('should match snapshot and have given text', () => {
    const testRenderer = renderer.create(<TestComponent text={'Hey! Press me!'} />);
    const testJson = testRenderer.toJSON();
    expect(testJson).toMatchSnapshot();
  });

  it('should match snapshot', () => {
    const testRenderer = renderer.create(<TestComponent />);
    const testJson = testRenderer.toJSON();
    expect(testJson).toMatchSnapshot();
  });
});

/**
 *  Vamos fazer alguns testes envolvendo o componente Linear Gradient e nosso componente
 */
const ThemedLinearGradient = props => (
  <LinearGradient colors={['#600081', '#FF008A']} style={{ flex: 1 }} {...props} />
);

/**
 * @toMatchSnapshot
 * + Faça um teste de snapshot com o componente lá encima substituindo o Container (vide prop) pelo ThemedLinearGradient
 */

describe('<TestComponent/> with LinearGradient', () => {
  it('should match snapshot', () => {
    const testRenderer = renderer.create(<TestComponent Container={ThemedLinearGradient} />);
    const testJson = testRenderer.toJSON();
    expect(testJson).toMatchSnapshot();
  });
});

/**
 * @findByProps
 * - utilizando o renderer encontre uma instancia dos seguintes componentes (com seus respectivos testIDs):
 *    + TouchableOpacity (#touchable)
 *    + BVLinearGradient (#container)
 *    + Text (#text)
 *
 * - use o selector findByProps que a root do testRenderer dá
 *    + renderer.create(...).root
 *    + testRoot.findByProps(node => node.testID === '#touchable')
 *      OU
 *    + testRoot.findByProps({ testID: '#touchable' })
 *
 * - Teste se essas instancias foram definidas (tem um catch nessa historia toda. Depois explico. Por enquanto usa o expect(...).toBeDefined())
 */
describe('<TestComponent/> the quest for the testID', () => {
  it('should render #touchable', () => {
    const testRenderer = renderer.create(<TestComponent Container={ThemedLinearGradient} />);
    const testRoot = testRenderer.root;
    const textInstance = testRoot.findByProps(node => node.testID === '#touchable');
    expect(textInstance).toBeDefined();
  });

  it('should render #container', () => {
    const testRenderer = renderer.create(<TestComponent Container={ThemedLinearGradient} />);
    const testRoot = testRenderer.root;
    const gradientInstance = testRoot.findByProps(node => node.testID === '#container');
    expect(gradientInstance).toBeDefined();
  });

  it('should render #text', () => {
    const testRenderer = renderer.create(<TestComponent Container={ThemedLinearGradient} />);
    const testRoot = testRenderer.root;
    const textInstance = testRoot.findByProps(node => node.testID === '#text');
    expect(textInstance).toBeDefined();
  });
});

/**
 * + Olha um novo componente! Vamos testar um pouco um componente dinâmico!
 * + Paz, amor e felicidade
 */

class HiderComponent extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      hidden: false
    };
  }

  toggle = () => {
    const { hidden } = this.state;
    this.setState({ hidden: !hidden });
  };

  render() {
    const { children } = this.props;
    const { hidden } = this.state;

    return (
      <RN.View testID={'#container'}>
        <RN.Button
          testID={'#button'}
          title={hidden ? 'Show' : 'Hide'}
          style={{ flex: 1 }}
          onPress={() => this.toggle()}
        />
        {!hidden && children}
      </RN.View>
    );
  }
}

/**
 * @longLongBrincadeira
 * + Normalmente Se deseja que os testes sejam curtos e simples, mas nesse nós queremos brincar com algumas coisas
 *
 *  - Crie uma instancia usando renderer.create(...) onde:
 *    + O componente hider tenha um componente do tipo RN.Text com "algum texto"
 *    + salve uma referencia para o root desse cara
 *
 *  - Use o root pra achar o container, o botão e todos os childrens. Use respectivamente: findByProps, findByProps e findAllByType
 *  OBS: Fica a dica: um RN.Button tem um children do tipo Text
 *
 *  - Depois disso: Agora procura esse texto children usando o findByProps({children: 'algum texto'})
 *
 *  - Depois disso tudo teste:
 *      + se a instancia do container tá definida
 *      + se a instancia do botão tá definida
 *      + se temos dois componentes do tipo Text
 *      + se a instancia do RN.Text que foi passada pra dentro tá definida
 *      + se o array[1] de instancias de texto é o 'algum texto' que vocês passaram
 *      + se nas props da instancia do botão temos na prop onPress uma função
 *
 *  - feito tudo isso agora vamos fazer o seguinte:
 *      + executa a função do onPress ali de antes. Bem no azar mesmo.
 *      + depois tenta achar o texto de qualquer forma e vê no que dá
 *
 *  - fica a dica: tu vai ter que usar um try catch finally:
 *      + defina um let qualquer ai para a instancia que vai ser procurada do RN.Text
 *      + no try procura por ela com findByProps
 *      + no catch dá um expect com qualquer coisa ai do e.stack,
 *              (depois vamos arrumar isso usando e.stack.includes('Error: No instances found with props'))
 *      + no finally espere que a referencia de let que vocês criaram seja .toBeUndefined()
 *
 * PS: Se vocês encontrem o cara é bruxaria. Porque quando EU rodei os testes tava funcionando o componente :v
 *
 */
describe('Hide Component', () => {
  it('exist or do not exist', () => {
    const testRenderer = renderer.create(
      <HiderComponent>
        <RN.Text>INSIRA SEU TEXTO AQUI</RN.Text>
      </HiderComponent>
    );
    const testRoot = testRenderer.root;

    const containerInstance = testRoot.findByProps(props => props.testID === '#container');
    const buttonInstance = testRoot.findByProps({ testID: '#button' });
    const textInstances = testRoot.findAllByType(RN.Text);
    const heyTextInstance = testRoot.findByProps({ children: 'INSIRA SEU TEXTO AQUI' });

    expect(containerInstance).toBeDefined();
    expect(buttonInstance).toBeDefined();
    expect(textInstances.length).toBe(2);
    expect(heyTextInstance).toBeDefined();
    expect(textInstances[1].props.children).toBe('INSIRA SEU TEXTO AQUI');
    expect(typeof buttonInstance.props.onPress).toBe('function');

    buttonInstance.props.onPress();
    let heyTextInstanceAfterClick;
    try {
      heyTextInstanceAfterClick = testRoot.findByProps({ children: 'INSIRA SEU TEXTO AQUI' });
    } catch (e) {
      expect(e.stack.includes('Error: No instances found with props')).toBe(true);
    } finally {
      expect(heyTextInstanceAfterClick).toBeUndefined();
    }
  });
});
